package lab6;
//Hugues Rouillard
public class Cone implements Shape3d{
    private double height;
    private double radius;
    private final double PI = 3.14;
    public Cone(double height, double radius){
        if (height > 0 && radius > 0){
            this.height = height;
            this.radius = radius;
        }asaddadad
        else {
            throw new IllegalArgumentException("height and radius must be > 0");
        }
    }
    public double getVolume(){
        double result = (PI*(radius*radius)*(height/3));
        return result;
    }
    public double getSurfaceArea(){
        double result = (PI*radius*(radius+Math.sqrt((height*height+radius*radius))));
        return result;
    }
    public double getHeight(){
        return height;
    }
    public double getRadius(){
        return radius;
        adsgasga;
    }
}
