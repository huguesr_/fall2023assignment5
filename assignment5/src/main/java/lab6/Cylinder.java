//Nguyen Dang Minh

package lab6;

public class Cylinder implements Shape3d{
    private double radius;
    private double height;
    private final double PI = 3.14;

    //Constructor
    public Cylinder(double radius, double height) {
        if (radius > 0 && height > 0){
            this.radius = radius;
            this.height = height;
        }
        else {
            throw new IllegalArgumentException("height and radius must be > 0");
        }
    }

    //Calculate volume
    public double getVolume() {
        return PI * Math.pow(radius, 2) * height;
    }

    //Calculate surface area
    public double getSurfaceArea() {
        return 2 * PI * radius * (radius + height);
    }

    public double getRadius() {
        return radius;
    }

    public double getHeight() {
        return height;
    }
}
