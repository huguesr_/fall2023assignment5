package lab6;

public interface Shape3d {
    double getVolume();
    double getSurfaceArea();
}
