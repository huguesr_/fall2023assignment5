// made by Vladimir De-Vreeze
package lab6;
public class Sphere implements Shape3d{
    private double radius;
    private final double PI = 3.14;

    // constructor
    public Sphere(double radius) {
        if (radius > 0) {
            this.radius = radius;
        } else {
            throw new IllegalArgumentException("radius mustbe greater than 0");
        }
    }

    // all the get methods
    public double getRadius() {
        return this.radius;
    }

    public double getSurfaceArea() {
        double result = 4*PI*this.radius*this.radius;
        return result;
    }

    public double getVolume() {
        double result = 4*PI*(this.radius*this.radius*this.radius)/3;
        return result;
    }
}
