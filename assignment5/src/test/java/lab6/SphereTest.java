package lab6;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
//Hugues Rouillard

public class SphereTest {
    @Test
    public void testConstructor(){
        Sphere nSphere = new Sphere(2);
        assertEquals("Should return radius", 2, nSphere.getRadius(), 0);
    }

    @Test
    public void testGetSurfaceArea(){
        Sphere nSphere = new Sphere(3);
        assertEquals("Should return surrface area", 113.1, nSphere.getSurfaceArea(), 0.1);
    }

    @Test
    public void testGetVolume(){
        Sphere nSphere = new Sphere(4);
        assertEquals("Should return volume", 268, nSphere.getVolume(), 0.1);
    }
}
