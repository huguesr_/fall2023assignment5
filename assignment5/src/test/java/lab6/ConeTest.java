//Nguyen Dang Minh

package lab6;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConeTest {
    @Test
    public void testGetVolume() {
        Cone cone = new Cone(2, 3);
        assertEquals(18.84, cone.getVolume(), 0.01);
    }
    @Test
    public void testGetSurfaceArea() {
        Cone cone = new Cone(2, 3);
        assertEquals(62.22429301, cone.getSurfaceArea(), 0.01);
    }
    @Test
    public void testConstructor() {
        Cone cone = new Cone(1, 1);
        assertEquals(1, cone.getHeight(), 0);
        assertEquals(1, cone.getRadius(), 0);
    }
}
