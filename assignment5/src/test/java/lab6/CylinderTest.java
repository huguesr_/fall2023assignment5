// code by: Vladimir De-Vreeze
package lab6;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CylinderTest {
    
    @Test
    public void volumeTest() {
        Cylinder cly = new Cylinder(2, 9);
        assertEquals("should return volume", 3.14*2*2*9, cly.getVolume(), 0.1);
    }

    @Test
    public void areaTest() {
        Cylinder cly = new Cylinder(5, 15);
        assertEquals("should return area", (2*3.14*5*15)+2*3.14*5*5, cly.getSurfaceArea(), 0.1);
    }

    @Test
    public void getRadiusTest() {
        Cylinder cly = new Cylinder(4, 95);
        assertEquals("constructor should set radius as 4", 4, cly.getRadius(), 0);
    }

    @Test
    public void getHeightTest() {
        Cylinder cly = new Cylinder(4, 2023);
        assertEquals("constructor should set height as 2023", 2023, cly.getHeight(), 0);
    }
}
